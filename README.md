# Irrigulr

Connects to the Agbinder SaaS Solution to provide an irrigation logging application.  That's all it does.

Currently the app only supports logging activities.  If you need to manipulate your data, use the website. Thanks.
