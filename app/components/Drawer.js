import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native'

import React, {Component} from 'react'

function MenuItem(props) {
    return (
        <View style={props.isActive ? styles.activeMenuItem : styles.menuItem}>
            <TouchableOpacity onPress={props.onPress}>
                <Text style={styles.text}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default class Drawer extends Component {

    render() {
        let menuItems = this.props.orchards
            .map((d, i) => <MenuItem key={i}
                                     text={d.name}
                                     onPress={()=>this.props.onSelectOrchard(d.name)}
                                     isActive={d.name === this.props.currentOrchard}/>)

        return (
            <View style={styles.drawer}>
                <View style={styles.drawerTitle}>
                    <Text style={styles.titleText}>{this.props.username}</Text>
                </View>
                <View style={styles.drawerContent}>
                    {menuItems}
                </View>

                <View style={styles.logoutSection}>
                    <TouchableOpacity onPress={this.props.onSignOut}>
                        <Text style={styles.signOutButton}>
                            logout
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    drawer: {
        flex: 1,
        justifyContent: 'space-between',
    },
    drawerTitle: {
        backgroundColor: '#fedac7',
        alignSelf: 'stretch'
    },
    drawerContent: {
        flex: 1,
        alignItems: 'flex-start'
    },
    logoutSection: {
        backgroundColor: '#945421',
        alignSelf: 'stretch'
    },
    titleText: {
        fontSize: 20,
        alignSelf: 'center'
    },
    menuItem: {
        borderColor: '#e7e7e7',
        borderBottomWidth: 1,
        alignSelf: 'stretch'

    },
    activeMenuItem: {
        borderColor: '#e7e7e7',
        borderBottomWidth: 1,
        alignSelf: 'stretch',
        backgroundColor: '#BB7701'
    },
    text: {
        fontSize: 18
    },
    signOutButton: {
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 24
    }

})