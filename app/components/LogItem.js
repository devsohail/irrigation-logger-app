import {View, Text} from 'react-native'
import React, {Component} from 'react'

export default function LogItem(props) {
    let duration = Math.ceil((props.endTime - props.startTime) / 1000)
    let mins = Math.floor(duration / 60);
    let seconds = duration % 60;
    let {width, height} = props.dimensions
    let basis = Math.min.call(null, width, height)
    return (
        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', width: basis, paddingBottom: 10}}>
            <View>
                <Text style={{fontSize: 20}}>
                    {props.startTime.toLocaleDateString()}
                </Text>
            </View>
            <View>
                <Text style={styles.logItem}>
                    {props.startTime.toLocaleTimeString()}
                </Text>
            </View>
            <View>
                <Text style={styles.logItem}>
                    {`${mins} mins ${seconds} secs`}
                </Text>
            </View>
        </View>
    )
}

const styles = {
    logItem: {
        fontSize: 20
    }

}
