import React, {
    Component
} from 'react'

import {
    Text,
    View,
    StyleSheet
} from 'react-native'

export default class NoOrchardsView extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textStyle}>
                    You do not have any orchards, this application cannot
                    be used to modify your data.  Please use the web based application
                    to add a orchard and try again.  Sorry for the inconvenience.
                </Text>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 15,
        color: '#854A52',
        marginHorizontal: 30
    }
})
