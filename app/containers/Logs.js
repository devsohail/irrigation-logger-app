import {
    StyleSheet,
    View,
    ListView,
    Dimensions,
    Platform,
    TouchableOpacity,
    Text
} from 'react-native'
import React, {Component} from 'react'

import {getAuthFetch, processTemplate} from '../util'
import LogItem from '../components/LogItem'

function updateLogs() {
    let path = processTemplate(this.props.logsEndpoint, {'orchard_id': this.props.currentOrchardId})
    let url = `${this.props.appEndpoint}/${path}`
    getAuthFetch(this.props.authToken).fetch(url)
        .then(res=>res.json())
        .then(data => {
            this.props.handleGetIrrigationLogs(data.irrigationEvents)
            this.setState({dataSource: ds.cloneWithRows(data.irrigationEvents)})
        })
        .catch(err=>console.error(err))
}

function rowHasChanged(r1, r2) {
    return r1.id !== r2.id;
}

const ds = new ListView.DataSource({rowHasChanged})

export default class Logs extends Component {

    constructor(props) {
        super(props)
        this.currentOrchardId = props.currentOrchardId
        this.state = {
            dataSource: ds.cloneWithRows([]),
            orientation: 'portrait'
        }
    }

    componentDidMount() {
        // here you can get the logs for the
        updateLogs.call(this)
    }

    componentDidUpdate() {
        if (this.props.currentOrchardId !== this.currentOrchardId) {
            updateLogs.call(this)
            this.currentOrchardId = this.props.currentOrchardId
        }
    }

    _renderRow(rowData, sectionId, rowId) {
        let {height, width} = Dimensions.get('window')
        return <LogItem startTime={new Date(Date.parse(rowData.startTime))}
                        endTime={new Date(Date.parse(rowData.endTime))}
                        dimensions={{height, width}}
                        rowId={rowId} />
    }

    render() {
        /* render this as a scroll view */
        let {height, width} = Dimensions.get('window')
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', width}}>
                    <Text style={styles.headers}>
                        Date
                    </Text>
                    <Text style={styles.headers}>
                        Time
                    </Text>
                    <Text style={styles.headers}>
                        Duration
                    </Text>
                </View>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}>
                </ListView>

            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    tableTitle: {
        fontSize: 22,
        alignSelf: 'center'
    },
    logItemRow: {

    },
    headers: {
        fontSize: 23,
        textAlign: 'center'
    }
})
