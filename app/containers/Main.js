import {
    AsyncStorage,
    DrawerLayoutAndroid,
    StyleSheet,
    View,
    ViewPagerAndroid,
    ToolbarAndroid,
    TouchableOpacity,
    Text
} from 'react-native'
import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {BackHandler} from '../util'
import NoOrchardsView from '../components/NoOrchardsView.js'
import Drawer from '../components/Drawer'
import Dashboard from './Dashboard'
import Logs from './Logs'
import {getAuthFetch, processTemplate} from '../util'

/*
  Main is a drawer layout -- this wraps either the dash or logline
 */


/**
 * get the orchard id given a set of orchard objects for the name given
 * @param orchards
 * @param name
 * @return id for orchard of given name
 */
function getOrchardIdFromName(orchards, name) {
    for (let orchard of orchards) {
        if (orchard.name === name) {
            return orchard.id;
        }
    }
}

function getOrchardFromName(orchards, name) {
    for (let orchard of orchards) {
        if (orchard.name === name) {
            return orchard
        }
    }
    return null
}

export default class Main extends Component {
    state = {
        currentOrchard: null,
        currentView: 'dashboard',
        currentOrchardIrrigationLogs: [],
        irrigationTimers: {
            /* orchardId: { timerState: state, timeStarted: time, timeEnded: time}*/
        }
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        /** only way to refer to registered handler without blowing your head off */
        this.backpress = BackHandler.addEventListener('hardwareBackPress', this.onBackPress.bind(this)).remove

        /** is there a current orchard? */
        await AsyncStorage.getItem('@irrigulr:currentOrchard')
            .then(data => {
                if (data)
                    this.setState({currentOrchard: data})
                else
                    throw new Error('current orchard is not set')
            })
            .catch(err => {
                console.log("previous orchard not set", err)
                if (this.props.orchards.length) {
                    this.setCurrentOrchard(this.props.orchards[0].name)
                }
            })
    }

    componentWillUnmount() {
        //if (this.state.wateringState === Symbol.keyFor('irrigulr:'))
        this.setState({currentView: ''}, function() {
            this.backpress();
        })
    }

    async _getOrchardDetails() {
        let data = await this.props.authFetch();
    }

    async setCurrentOrchard(orchardName) {
        this.setState({currentOrchard: orchardName})
        await AsyncStorage.setItem('@irrigulr:currentOrchard', orchardName)
    }

    async submitNewIrrigationEvent(orchardId) {
        let path = processTemplate(this.props.urls.orchardIrrigationEvents, {'orchard_id': orchardId})
        let now = (new Date()).toJSON()
        let data = {
            'startTime': this.state.irrigationTimers[orchardId].startTime,
            'endTime': now
        }
        let headers = new Headers({
            "Content-Type": "application/json"
        })

        let reqProps = {
            method: "POST",
            body: JSON.stringify(data),
            headers
        }
        let resp = await getAuthFetch(this.props.authToken)
            .fetch(`${this.props.endpoint}/${path}`, reqProps)
            .then((res) => res.json())

        return resp
    }

    updateIrrigationState(newWateringState) {
        let irrigationTimers = this.state.irrigationTimers
        let orchardId = getOrchardIdFromName(this.props.orchards, this.state.currentOrchard)
        let newIrrigationTimers = {}
        let currentIrrigationState = irrigationTimers[orchardId]
            ? irrigationTimers[orchardId].timerState
            : Symbol.for('irrigulr:notWatering')

        // clone the existing irrigationTimers
        for (let k in irrigationTimers) {
            newIrrigationTimers[k] = Object.assign({}, irrigationTimers[k])
        }

        switch(currentIrrigationState) {
            case Symbol.for('irrigulr:notWatering'):
                if (newWateringState === Symbol.for('irrigulr:watering')) {
                    newIrrigationTimers[orchardId] = {
                        timerState: Symbol.for('irrigulr:watering'),
                        startTime: new Date(),
                        endTime: null
                    }
                    this.setState({irrigationTimers: newIrrigationTimers})
                } else {
                    console.log('state transition not permitted')
                }
                break
            case Symbol.for('irrigulr:watering'):
                if (newWateringState === Symbol.for('irrigulr:cancelling')) {
                    newIrrigationTimers[orchardId] = {
                        timerState: Symbol.for('irrigulr:notWatering'),
                        startTime: null,
                        endTime: null
                    }
                    this.setState({irrigationTimers: newIrrigationTimers})
                } else if (newWateringState === Symbol.for('irrigulr:saving')) {
                    newIrrigationTimers[orchardId] = {
                        timerState: Symbol.for('irrigulr:notWatering'),
                        startTime: newIrrigationTimers[orchardId].startTime,
                        endTime: newIrrigationTimers[orchardId].endTime
                    }
                    this.setState({irrigationTimers: newIrrigationTimers})
                    this.submitNewIrrigationEvent(orchardId)
                        .then(()=> {
                            newIrrigationTimers[orchardId] = {
                                timerState: Symbol.for('irrigulr:notWatering'),
                                startTime: null,
                                endTime: null
                            }
                            this.setState({irrigationTimers: newIrrigationTimers})
                        })
                        .catch( err => console.log('unable to save data', err))
                } else {
                    console.log('state transition not permitted')
                }
        }
    }

    getView(viewname) {
        let view;
        let currentOrchardId = getOrchardIdFromName(this.props.orchards, this.state.currentOrchard)
        switch(viewname) {
            case 'dashboard':
                view = <Dashboard
                    currentOrchard={this.state.currentOrchard}
                    onSwipe={() => this.setView('logs')}
                    currentOrchardTimerState={this.state.irrigationTimers[currentOrchardId]}
                    onSaveButtonPress={(newWateringState) => this.updateIrrigationState(newWateringState)}
                    onCancelButtonPress={(newWateringState) => this.updateIrrigationState(newWateringState)}
                    onIrrigationButtonPress={(newWateringState) => this.updateIrrigationState(newWateringState)} />
                break
            case 'no-orchard':
                view = <NoOrchardsView/>
                break
            case 'logs':
                view = <Logs
                    currentOrchardId={currentOrchardId}
                    currentOrchard={this.state.currentOrchard}
                    authToken={this.props.authToken}
                    logsEndpoint={this.props.urls.orchardIrrigationEvents}
                    appEndpoint={this.props.endpoint}
                    handleGetIrrigationLogs={(data) => this.handleGetIrrigationLogs(data)}
                    currentOrchardLogs={this.state.currentOrchardIrrigationLogs}
                />
                break
            default:
                view = this.getView('dashboard')
        }
        return view
    }

    setView(viewId) {
       this.setState({currentView: viewId})
    }

    onBackPress() {
        if (this.state.currentView === 'logs'){
            this.setView('dashboard')
            return true
        } else {
            return false
        }
    }

    handleGetIrrigationLogs(logs) {
        this.setState({currentOrchardIrrigationLogs: logs})
    }

    handleSelectOrchard(orchard) {
        this.setCurrentOrchard(orchard)
    }

    render() {
        let noOrchards = !this.props.orchards.length;
        let view = noOrchards ? this.getView('no-orchard') : this.getView(this.state.currentView)
        let self = this;
        let navigationView = <Drawer
            username={this.props.currentUser.email}
            onSignOut={() => {
                self.backpress()
                this.props.onSignOut()
            }}
            onSelectOrchard={(orchard)=> this.handleSelectOrchard(orchard)}
            currentOrchard={this.state.currentOrchard}
            orchards={this.props.orchards} />


        return (

            <DrawerLayoutAndroid
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}
            >
                <ToolbarAndroid

                    style={{height:100,   backgroundColor:'#ABCDEF', alignSelf: 'stretch', alignItems: 'center'}}
                    actions={[{title: 'SETTINGS', show: 'never'}]}
                    title={`Irrigulr - ${this.state.currentOrchard}`}
                />
                {view}
            </DrawerLayoutAndroid>
        )
    }

}

Main.propTypes = {
    onSignOut: PropTypes.func.isRequired
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    signOutButton: {
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 24,
        alignItems: 'center'
    }
})
