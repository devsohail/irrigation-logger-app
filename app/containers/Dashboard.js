import {
    AsyncStorage,
    Button,
    Dimensions,
    PanResponder,
    StyleSheet,
    TouchableOpacity,
    View,
    Text
} from 'react-native'
import React, {Component} from 'react'


export default class Dashboard extends Component {

    constructor(props) {
        super(props)
        let elapsedTime;
        let startTime;
        if (props.currentOrchardTimerState) {
            startTime = props.currentOrchardTimerState.startTime
            elapsedTime = Math.floor((new Date() - startTime) / 1000)
        } else {
            elapsedTime = 0
        }
        this.state = {
            elapsedTime
        }
    }

    setupTimer() {
        if (this.props.currentOrchardTimerState
            && this.props.currentOrchardTimerState.timerState === Symbol.for('irrigulr:watering')
            && this.timerId === null) {
            this.timerId = setInterval(
                () => this.tick(),
                250)
        } else {
            if (this.timerId !== null) {
                this.tearDownTimer()
            }
            this.timerId = null;
        }
    }

    tearDownTimer() {
        clearInterval(this.timerId)
        this.timerId = null
        this.setState({elapsedTime: 0})
    }

    componentDidMount() {
       this.setupTimer()
    }

    componentDidUpdate() {
        let timerState;
        if (this.props.currentOrchardTimerState) {
           timerState = this.props.currentOrchardTimerState.timerState;
           if (timerState === Symbol.for('irrigulr:watering') && this.timerId === null) {
               this.setupTimer()
           } else if (timerState === Symbol.for('irrigulr:notWatering') && this.timerId !== null) {
               this.tearDownTimer()
           }
        }
    }

    componentWillUnmount() {
        if (this.timerId) {
            this.tearDownTimer()
        }
    }

    tick() {
        let elapsedTime
        if (this.props.currentOrchardTimerState) {
            startTime = this.props.currentOrchardTimerState.startTime
            elapsedTime = Math.floor((new Date() - startTime) / 1000)
        } else {
            elapsedTime = 0
        }
        this.setState({elapsedTime})
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => {
                let {height, width} = Dimensions.get('window')
                return gestureState.moveX > width / 2
            },
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                let {height, width} = Dimensions.get('window')
                return gestureState.moveX > width / 2
            },
            onPanResponderGrant: (evt, gestureState) => {
                console.log("moving")
            },

            onPanResponderRelease: (evt, gestureState) => {
                let {height, width} = Dimensions.get('window')
                let percentageChange = (gestureState.dx / width) * 100
                if (percentageChange < -20.0)
                    this.props.onSwipe()
            }

        })
    }

    render() {
        let timerState = this.props.currentOrchardTimerState
            ? this.props.currentOrchardTimerState.timerState
            : Symbol.for('irrigulr:notWatering')
        let controlsBlock = timerState === Symbol.for('irrigulr:notWatering')
            ?
            <View style={styles.controlsBlock}>
                <TouchableOpacity title="Start"
                                  onPress={()=>this.props.onIrrigationButtonPress(Symbol.for('irrigulr:watering'))}>
                    <Text style={styles.control} >start</Text>
                </TouchableOpacity>
            </View>
            :
            <View style={styles.controlsBlock}>
                <TouchableOpacity title="Cancel"
                                  onPress={()=>this.props.onCancelButtonPress(Symbol.for('irrigulr:cancelling'))}>
                    <Text style={styles.control} >cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity title="Save"
                                  onPress={()=>this.props.onSaveButtonPress(Symbol.for('irrigulr:saving'))}>
                    <Text style={styles.control} >save</Text>
                </TouchableOpacity>
            </View>

        let mins = Math.floor(this.state.elapsedTime / 60)
        let seconds = this.state.elapsedTime % 60
        return (
            <View style={styles.container} {...this._panResponder.panHandlers}>
                <View style={styles.timerBlock}>
                    <View>
                       <Text style={{fontSize: 125}}>{`${mins < 10 ? '0' : ''}${mins}:${seconds < 10 ? '0' : ''}${seconds}`}</Text>
                    </View>
                </View>
                {controlsBlock}
            </View>
        )

    }
}

let styles = StyleSheet.create({
    main: {
        fontSize: 32
    },
    container: {
        flex: 2,
        alignItems: 'center',
        backgroundColor: '#eaffff',
        justifyContent: 'flex-start'
    },
    timerBlock: {
        marginTop: 25,
        flex: 10
    },
    controlsBlock: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        width: 200
    },
   control: {
        borderRightColor: 'black',
        borderWidth: 2,
        borderColor: '#476a73',
        backgroundColor: '#8fdaeb',
        textAlign: 'center',
        marginBottom: 10,
        paddingTop: 5,
        marginRight: 20,
        marginLeft: 20,
        width: 80

    },
    controlText: {
        textAlign: 'center'
    }
});
