import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import React, { Component } from "react";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';

import {requestFactory, processTemplate, getAuthFetch} from '../util'
import Main from './Main.js'
import Login from './Login.js'

/**
 * main app component
 */
class App extends Component {

    endPoint = "http://192.168.2.11:6544";

    state = {
        currentUser: {},
        authToken: null,
        urls: {},
        orchards: []
    };

    constructor(props) {
        super(props)
        AsyncStorage.getItem('@irrigulr:token')
            .then((data) => this.setState({authToken: data}))
            .catch(err => console.log(err))
    }

    async getPaths() {
        let request = new Request(this.endPoint, {
            headers: {
                accept: 'application/json'
            }
        })
        return fetch(request)
            .then(res=>res.json())
            .then(json => {
                this.setState({urls: json})
            })
            .catch(err => console.error(`API: is not up, load api at the specified endpoint\n${err}`))
    }

    async componentDidMount() {
        // request '/' to get list of relations, in particular, the server
        // sends the relations `login` and `oauthCallback`
        await this.getPaths()

        // mount the signin button
        this._initSignin();
    }

    async _initSignin() {
        console.log(this.state)
        try {
            await GoogleSignin.hasPlayServices({autoResolve: true})
            await GoogleSignin.configure({
                scopes: ["https://www.googleapis.com/auth/plus.login"],
                webClientId: "807476606136-dvguimnckdtdkc9jotcp181kpuehka0d.apps.googleusercontent.com",
                offlineAccess: true

            })
        } catch(err) {
            console.log("Play services error", err.code, err.message);
        }
    }

    async _setSignIn() {
        let headers = new Headers()
        let requestProperties = { method: 'POST'}
        let user;
        console.log(this.state)
        return GoogleSignin.signIn()
            .then((res)=> {
                user = res
                let reqData = {
                    serverAuthCode: res.serverAuthCode,
                    email: res.email,
                    name: res.name,
                    scopes: res.scopes
                }
                headers.append('Content-Type', 'application/json')
                requestProperties.body = JSON.stringify(reqData)
                requestProperties.headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
                return fetch(`${this.endPoint}/${this.state.urls.oauthCallback}`, requestProperties)
            })
            .then(res => res.json())
            .then(json => {
                let user = {
                    username: json.username,
                    email: json.email
                }
                // store the server auth code in local storage
                this.setState({currentUser: user})
                this.setState({authToken: json.token})
                this.setState({orchards: json.orchards})
                return AsyncStorage.setItem('@irrigulr:token', json.token)
            })
            .catch((err)=>console.log('bad login', err))
            .done();
    }

    async getUserData(username) {
        let t = this.state.urls.userInfoTemplate
        let tProcessed = processTemplate(t, {username})
        let url = `${this.endPoint}/${tProcessed}`
        await getAuthFetch(this.state.authToken).fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState({orchards: json.orchards})
            })
    }

    _setSignOut() {
        this.setState({currentUser: {}});
    }

    render() {
        let {currentUser, orchards} = this.state
        var view = !currentUser.username
        ?
            <View style={styles.container}>
                <Login
                    onSignIn={() => this._setSignIn() }
                />
            </View>

        :
            <Main
                authToken={this.state.authToken}
                currentUser={currentUser}
                orchards={orchards}
                endpoint={this.endPoint}
                onSignOut={() => this._setSignOut() }
                urls={this.state.urls}
                style={styles.container}
            />
        return (
                view
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});

export default App;
