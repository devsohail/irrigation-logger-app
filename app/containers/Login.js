import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text
} from 'react-native'
import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {GoogleSigninButton} from 'react-native-google-signin'

export default class Login extends Component {

    render() {

        return (
            <View>
                <Text style={styles.lead}>
                    Irrigulr
                </Text>
                <GoogleSigninButton
                    style={styles.signinButton}
                    size={GoogleSigninButton.Size.Standard}
                    color={GoogleSigninButton.Color.Light}
                    onPress={ this.props.onSignIn}
                />
            </View>
        )
    }
}

Login.propTypes = {
    onSignIn: PropTypes.func.isRequired

}


let styles = StyleSheet.create({
    lead: {
        fontSize: 60,
        textAlign: 'center',
        marginBottom:20,
        fontWeight: "bold"
    },
    signinButton: {
        height: 48,
        width: 230
    },
})