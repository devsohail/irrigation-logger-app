import {BackAndroid} from 'react-native';

export function requestFactory(options) {
    return function (url) {
        return new Request(url, options)
    }
}

/**
 * processes a template
 * @param template
 * @param parameters
 * @returns {*}
 */
export function processTemplate(template, parameters) {
    let s = template;
    let regExpObj;
    Object.keys(parameters).forEach(param => {
        let data = parameters[param]
        regExpObj = new RegExp('{' + param + '}');
        if (! regExpObj.test(s) ) {
            throw new Error(`string:${s} does not contain parameter: ${param}`)
        }
        s = regExpObj[Symbol.replace](s, data)
    })
    return s;
}

function mergeHeaders(destHdrs, srcHdrs) {
    if (srcHdrs.entries) {
        for (let p of srcHdrs) {
            destHdrs[p[0]]  = destHdrs[p[1]]
        }
    } else {
        for (let k in srcHdrs) {
            if (srcHdrs.hasOwnProperty(k))
                destHdrs[k] = srcHdrs[k]
        }
    }
    return destHdrs;
}

export function getAuthFetch(username, password='') {
    let credentials = btoa(`${username}:${password}`)
    return {
        fetch: async (url, options = null) => {
            options = options || {}
            let authHeaders = {Authorization: `Basic ${credentials}`}
            if (options.headers) {
                mergeHeaders(authHeaders, options.headers)
            }
            options.headers = authHeaders
            return fetch(url, options)
        }
    }
}

export let BackHandler = {
    exitApp: () => BackAndroid.exitApp(),
    addEventListener: (evt, handler) => BackAndroid.addEventListener(evt, handler),
    removeEventListener: (evt, handler) => BackAndroid.removeEventListener(evt, handler)
}
